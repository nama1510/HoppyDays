extends AnimatedSprite

var taken = false


func _on_Area2D_body_entered(body):
	if taken == false:
		taken = true
		Global.GameState.coin_Up()
		$Coin_SFX.play()
		$AnimationPlayer.play("die")
	
func die():
	queue_free()
