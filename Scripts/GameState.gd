extends Node2D


export var starting_lives = 3
export var coin_target_for_extra_life = 15
var lives
var coins = 0

onready var GUI = Global.GUI

func _ready():
	Global.GameState = self
	lives = starting_lives
	update_GUI()


func update_GUI():
	GUI.update_GUI(lives, coins)


func hurt():
	lives -= 1
	update_GUI()
	Global.Player.hurt()
	GUI.animate("Hurt")
	if lives < 0:
		end_game()

func coin_Up():
	coins += 1
	GUI.animate("CoinPulse")
	if coins % coin_target_for_extra_life == 0:
		lives += 1
		GUI.animate("LifePulse")
	update_GUI()
	
func _on_Portal_body_entered(body):
	win_game()

func end_game():
	get_tree().change_scene(Global.GameOver)
	
func win_game():
	get_tree().change_scene(Global.Victory)
