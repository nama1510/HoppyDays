extends KinematicBody2D

const SPEED = 750
const JUMP_SPEED = -1500
const GRAVITY = 3600
const UP = Vector2(0,-1)
const JUMP_BOOST = 2;

var motion = Vector2()

export var world_limit = 3000

func _ready():
	Global.Player = self


func _physics_process(delta):
	move_and_slide(motion, UP)
	fall(delta)
	run()
	jump()


func _process(delta):
	update_animation(motion)

func update_animation(motion):
	$AnimatedSprite.update(motion)

func fall(delta):
	if is_on_floor() || is_on_ceiling():
		motion.y = 150
	else:
		motion.y += GRAVITY * delta
		
	if position.y > world_limit:
		Global.GameState.end_game()
	

func jump():
	if is_on_floor() && Input.is_action_just_pressed("ui_up"):
		motion.y = JUMP_SPEED
		Global.Jump_SFX.play()

func boost():
	motion.y = JUMP_SPEED * JUMP_BOOST

func hurt():
	motion.y = JUMP_SPEED
	Global.Pain_SFX.play()

func run():
	if Input.is_action_just_pressed("ui_up") == false:
		if Input.is_action_pressed("ui_right") && Input.is_action_pressed("ui_left") == false:
			motion.x = SPEED
		elif Input.is_action_pressed("ui_left") && Input.is_action_pressed("ui_right") == false:
			motion.x = -SPEED
		else:
			motion.x = 0


