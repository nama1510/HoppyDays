extends AnimatedSprite

func update(motion):
	if motion.y < 0:
		play("jump")
		set_flip_h(false)
	elif motion.x > 0:
		set_flip_h(false)
		play("run")
	elif motion.x < 0:
		set_flip_h(true)
		play("run")
	else:
		set_flip_h(false)
		play("idle")