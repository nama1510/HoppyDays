extends Area2D

func _ready():
	pass


func _on_Jump_Pad_body_entered(body):
	Global.Player.boost()
	$AnimatedSprite.play("Spring")
	$Timer.start()


func _on_Timer_timeout():
	$AnimatedSprite.play("Idle")
