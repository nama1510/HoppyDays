extends Node

var GameState
var Player
var GUI
var Jump_SFX
var Pain_SFX

#Scene Locations
var Level1 = "res://Scenes/Levels/Level1.tscn"
var GameOver = "res://Scenes/Levels/GameOver.tscn"
var Victory = "res://Scenes/Levels/Victory.tscn"
var Lightning = "res://Scenes/Lightning.tscn"

